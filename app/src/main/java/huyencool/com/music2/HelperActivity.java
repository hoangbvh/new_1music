package huyencool.com.music2;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import java.util.ArrayList;
import huyencool.com.music2.model.BaiHat;

public class HelperActivity extends BroadcastReceiver {
    public static ArrayList<BaiHat> listBaiHat;
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Bundle extras = intent.getExtras();
        if(extras!=null){
            int pos = extras.getInt("pos");
            sendBroadcast(context, action,pos);
        }
    }
    private void sendBroadcast (Context ctx, String action,int pos){
        Intent intent = new Intent ("updateFromNotification"); //put the same message as in the filter you used in the activity when registering the receiver
        intent.putExtra("action",action);
        intent.putExtra("pos",pos);
        LocalBroadcastManager.getInstance(ctx).sendBroadcast(intent);
    }

}
