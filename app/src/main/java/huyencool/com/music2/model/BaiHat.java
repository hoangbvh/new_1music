package huyencool.com.music2.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class BaiHat implements Serializable {
    String mStt;
    String mTenbaihat;
    String mTencasi;
    String mTime;
    String uri;
    String anh;

    public BaiHat()
    {
        super();
    }
    public BaiHat(String mStt, String mTenbaihat, String mTencasi, String mTime, String uri,String anh)
    {
        super();
        this.mStt=mStt;
        this.mTenbaihat=mTenbaihat;
        this.mTencasi=mTencasi;
        this.mTime=mTime;
        this.uri=uri;
        this.anh=anh;
    }

    protected BaiHat(Parcel in) {
        mStt = in.readString();
        mTenbaihat = in.readString();
        mTencasi = in.readString();
        mTime = in.readString();
        uri = in.readString();
        anh = in.readString();
    }

    public void setmStt(String mStt)
    {
        this.mStt=mStt;
    }
    public String getmStt()
    {
        return mStt;
    }
    public void setmTenbaihat(String mTenbaihat1)
    {
        this.mTenbaihat=mTenbaihat;
    }
    public String  getmTenbaihat()
    {
        return mTenbaihat;
    }
    public void setmTencasi(String mTencasi)
    {
        this.mTencasi=mTencasi;
    }
    public String getmTencasi()
    {
        return mTencasi;
    }
    public void setmTime(String mTime)
    {
        this.mTime=mTime;
    }
    public String getmTime()
    {
        return mTime;
    }
    public String getAnh()
    {
        return anh;
    }
    public void setAnh(String anh)
    {
        this.anh=anh;
    }
    public void setUri(String id)
    {
        this.uri=id;
    }
    public String getUri()
    {
        return uri;
    }


    public static void saveToShared(Context ctx, ArrayList<BaiHat> l,String typeMusic) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences("huyencool.com.music2", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(l);
        editor.putString(typeMusic,json);
        editor.apply();
        editor.commit();
    }
    public static ArrayList<BaiHat> loadFromShared(Context ctx,String typeMusic){
        SharedPreferences sharedPreferences = ctx.getSharedPreferences("huyencool.com.music2",Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(typeMusic,null);
        ArrayList<BaiHat> l;
        Type type = new TypeToken<ArrayList<BaiHat>>(){}.getType();
        l = gson.fromJson(json,type);
        return l;
    }
}

