package huyencool.com.music2.view.new_interface;

import huyencool.com.music2.model.BaiHat;

public interface MusicPlayListener {
    void onPause();
    void onPlay();
    void onNext(int positionActive);
    void onPrev(int positionActive);
    void onGetTime(BaiHat baiHat);
    void seekTo(BaiHat baiHat,int time);
}
