package huyencool.com.music2.view.new_interface;

import android.view.View;

import java.util.ArrayList;

import huyencool.com.music2.model.BaiHat;

public interface ViewPlayerListener {
    void onTouchToOpen();
    void onCollapseView();
    void updateListMusic(ArrayList<BaiHat> listMusic);
}
