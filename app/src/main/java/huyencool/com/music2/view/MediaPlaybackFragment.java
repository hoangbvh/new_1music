package huyencool.com.music2.view;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import huyencool.com.music2.Library;
import huyencool.com.music2.MyNotification;
import huyencool.com.music2.R;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.model.Constant;
import huyencool.com.music2.view.new_interface.MusicPlayListener;
import huyencool.com.music2.view.new_interface.ViewPlayerListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class MediaPlaybackFragment extends Fragment {
    private BaiHat baiHatActive;
    ArrayList<BaiHat> listBaiHat;
    TextView nameMusic;
    TextView artist;
    ImageView smallImageMusic;
    ImageView bigImageMusic;
    ImageView nextMusicButton;
    ImageView backToListButton;
    SeekBar seekBar;
    TextView timeBatdau;
    TextView timeKetthuc;
    ImageView pausePlayButtom;
    ImageView playButton;       //button play in header
    ImageView prevButton;
    ImageView likeButton;
    ImageView disLikeButton;
    ImageView moreButton;
    int sTime = 1000;
    int eTime = 20000000;
    boolean isPlaying = true;
    int positionActive = 0;

    //
    Boolean isFavorite = false;
    Boolean isDisLike = false;
    LinearLayout viewPlayButton;
    LinearLayout viewBackAndMoreButton;
    private ArrayList<BaiHat> listFavorite = new ArrayList<>();
    private ArrayList<BaiHat> listDisLike = new ArrayList<>();
    //
    ConstraintLayout headerMediaPlayer;
    ViewPlayerListener viewPlayerListener;

    //
    MusicPlayListener musicPlayListener;
    public void setListBaiHat(ArrayList<BaiHat> list){
        this.listBaiHat = list;
    }
    public void setPositionActive(int positionActive){
        this.positionActive = positionActive;
    }
    public void setMusicPlayListener(MusicPlayListener musicPlayListener){
        this.musicPlayListener = musicPlayListener;
    }

    public void setViewPlayerListener(ViewPlayerListener viewPlayerListener){
        this.viewPlayerListener = viewPlayerListener;
    }
    public MediaPlaybackFragment(){

    }
    public void setBaiHatActive(BaiHat baiHatActive){
        this.baiHatActive = baiHatActive;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (baiHatActive != null)
        musicPlayListener.onGetTime(baiHatActive);
    }

    private BroadcastReceiver bReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            sTime = Integer.parseInt(intent.getStringExtra("currentTime"));
            eTime = Integer.parseInt(intent.getStringExtra("maxTime"));
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    seekBar.setProgress(sTime/1000);
                    seekBar.setMax(eTime/1000);
                    long phutKetthuc = TimeUnit.MILLISECONDS.toMinutes((long) eTime);
                    long giayKetthuc = TimeUnit.MILLISECONDS.toSeconds((long) eTime) - (phutKetthuc * 60);
                    timeKetthuc.setText(String.format("%d : %d", phutKetthuc, giayKetthuc));
                }
            });
        }
    };
    private BroadcastReceiver bReceiverFromNotification = new BroadcastReceiver(){
        // nhan du liệu từ thanh thông báo
        //click next pause prev
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            int pos = intent.getIntExtra("pos",-1);
            if(action != null){
                switch (action) {
                    case "prev": {
                        BaiHat baiHat = listBaiHat.get(pos);
                        positionActive = pos;
                        updateUI(baiHat);
                        musicPlayListener.onPrev(positionActive);
                        break;
                    }
                    case "next": {
                        BaiHat baiHat = listBaiHat.get(pos);
                        positionActive = pos;
                        updateUI(baiHat);
                        musicPlayListener.onNext(positionActive);
                        break;
                    }
                    case "pause":
                        isPlaying = true;
                        updateState();
                        break;
                    case "play":
                        isPlaying = false;
                        updateState();
                        break;
                }
            }


        }
    };
    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(bReceiver, new IntentFilter("message")); // dki vs service
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(bReceiverFromNotification, new IntentFilter("updateFromNotification"));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(bReceiver); // huy dki service
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(bReceiverFromNotification);
        isPlaying = false;
    }

    private void changeSeekBar(int s, int e){
        if (!isPlaying)
            return;
        seekBar.setProgress(sTime / 1000); // hien thi time chay
        if (sTime/1000 >= eTime/1000) {

            musicPlayListener.onNext(positionActive);

            int p = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
            positionActive = p;
            sTime = 1000;
            updateUI(listBaiHat.get(p));
        }
        else{
            sTime = s + 1000;
            // gán lại thời gian của bài hát đang hát bi dung
            eTime = e;
            final int tmp = s + 1000;
            final long phutBatdau = TimeUnit.MILLISECONDS.toMinutes((long) sTime);
            final long giayBatdau = TimeUnit.MILLISECONDS.toSeconds((long) sTime) - (phutBatdau * 60);
            new Handler().postDelayed(new Runnable() { // hàm chơ 1 giây sau thực hiện tiếp
                @Override
                public void run() {
                    if (tmp == sTime){
                        timeBatdau.setText(String.format("%d : %d", phutBatdau, giayBatdau));
                    }
                    changeSeekBar(sTime,eTime);
                }
            },1000);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_media_playback2, container, false);
         nameMusic = view.findViewById(R.id.musicName);
         artist = view.findViewById(R.id.musicArtist);
         smallImageMusic = view.findViewById(R.id.smallImageMusic);
         bigImageMusic = view.findViewById(R.id.musicBigImageView);
         seekBar = view.findViewById(R.id.seekBar);
         timeBatdau = view.findViewById(R.id.startTimeTextView);
         timeKetthuc = view.findViewById(R.id.endTimeTextView);
         pausePlayButtom = view.findViewById(R.id.pausePlayButton);
         nextMusicButton = view.findViewById(R.id.nextMusicButton);
        prevButton = view.findViewById(R.id.prevButton);
        likeButton = view.findViewById(R.id.likeButton);
        disLikeButton = view.findViewById(R.id.disLikeButton);
        moreButton = view.findViewById(R.id.moreButton);
         // header view
        viewBackAndMoreButton = view.findViewById(R.id.viewBackAndMoreButton);
        viewPlayButton = view.findViewById(R.id.viewPlayButton);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            viewBackAndMoreButton.setVisibility(LinearLayout.GONE);
            viewPlayButton.setVisibility(View.VISIBLE);
        }
        else{
            viewBackAndMoreButton.setVisibility(LinearLayout.VISIBLE);
            viewPlayButton.setVisibility(View.GONE);
        }
        loadFromCache();            // laay bai hat da luu vao yêu thích và không yêu thích từ bộ nhớ
        changeSeekBar(sTime,eTime);
        if (baiHatActive == null){
            // duyệt tìm bài hát đang hát có trong list yêu thích hay ko, nếu có thì thay icon like thành liked
            for (BaiHat bh : listFavorite){
                if (bh.getUri().equals(baiHatActive.getUri())){
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_selected);
                    isFavorite = true;
                    break;
                }
            }
            // duyệt tìm bài hát đang hát có trong list không yêu thích hay ko, nếu có thì thay icon unlike thành unliked

            for (BaiHat bh : listDisLike){
                if (bh.getUri().equals(baiHatActive.getUri())){
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_selected);
                    isDisLike = true;
                    break;
                }
            }
        }

        likeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (isFavorite){
                    listFavorite.remove(baiHatActive);
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_default);
                }
                else{
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_selected);
                    listFavorite.add(baiHatActive);
                }
                if (isDisLike){
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_default);
                    listDisLike.remove(baiHatActive);
                }
                // luu lai danh sach bai hat vao bộ nhớ bài yêu thích và không yêu thích
                BaiHat.saveToShared(getContext(),listDisLike,Constant.DIS_LIKE);
                BaiHat.saveToShared(getContext(),listFavorite,Constant.FAVORITE_MUSIC);
            }
        });
        disLikeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (isDisLike){
                    listDisLike.remove(baiHatActive);
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_default);
                }
                else{
                    disLikeButton.setImageResource(R.drawable.ic_thumbs_down_selected);
                    listDisLike.add(baiHatActive);
                }
                if (isFavorite){
                    likeButton.setImageResource(R.drawable.ic_thumbs_up_default);
                    listFavorite.remove(baiHatActive);
                }
                // luu lai danh sach bai hat vao bộ nhớ bài yêu thích và không yêu thích
                BaiHat.saveToShared(getContext(),listDisLike,Constant.DIS_LIKE);
                BaiHat.saveToShared(getContext(),listFavorite,Constant.FAVORITE_MUSIC);
            }
        });
        moreButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getContext(),view);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        viewPlayerListener.onCollapseView();
                        switch (menuItem.getItemId()) {
                            case R.id.listFavorite:
                                ArrayList<BaiHat> listFavorite = getFavoriteMusic();
                                viewPlayerListener.updateListMusic(listFavorite);
                                return true;
                            case R.id.listDislike:
                                ArrayList<BaiHat> listDisLike = getDislikeMusic();
                                viewPlayerListener.updateListMusic(listDisLike);
                            default:
                                return false;
                        }
                    }
                });
                MenuInflater menuInflater = popupMenu.getMenuInflater();
                menuInflater.inflate(R.menu.menu_media, popupMenu.getMenu());
                popupMenu.show();
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                musicPlayListener.seekTo(baiHatActive,seekBar.getProgress() * 1000);
                sTime = seekBar.getProgress() * 1000;
                final long phutBatdau = TimeUnit.MILLISECONDS.toMinutes((long) sTime);
                final long giayBatdau = TimeUnit.MILLISECONDS.toSeconds((long) sTime) - (phutBatdau * 60);
                timeBatdau.setText(String.format("%d : %d", phutBatdau, giayBatdau));

            }
        });
         //header media player
        headerMediaPlayer = view.findViewById(R.id.headerMediaPlayer);
        headerMediaPlayer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                viewBackAndMoreButton.setVisibility(LinearLayout.VISIBLE);
                viewPlayButton.setVisibility(View.GONE);
                viewPlayerListener.onTouchToOpen();
            }
        });
        //
        backToListButton = view.findViewById(R.id.backToListButton);
        backToListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewBackAndMoreButton.setVisibility(LinearLayout.GONE);
                viewPlayButton.setVisibility(View.VISIBLE);
                viewPlayerListener.onCollapseView();
            }
        });
        playButton = view.findViewById(R.id.playButton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateState();

            }
        });

         pausePlayButtom . setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 updateState();
             }
         });
         nextMusicButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 int p = (positionActive == listBaiHat.size() - 1) ? 0 : positionActive + 1;
                 positionActive = p;
                 updateUI(listBaiHat.get(p));
                 musicPlayListener.onNext(positionActive);

             }
         });
        prevButton.setOnClickListener(new View.OnClickListener( ){
            @Override
            public void onClick(View view) {
                int p = (positionActive == 0) ? listBaiHat.size() - 1 : positionActive - 1;
                positionActive = p;
                updateUI(listBaiHat.get(p));
                musicPlayListener.onPrev(positionActive);

            }
        });
        if (baiHatActive != null)
        updateUI(baiHatActive);
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);

    }

    private void updateState(){
        MyNotification.createNotification(getContext(),baiHatActive,!isPlaying);
        if(isPlaying){
            musicPlayListener.onPause();
            playButton.setImageResource(R.drawable.ic_play_black_round);
            pausePlayButtom.setImageResource(R.drawable.ic_play_black_round);
            isPlaying = false;
        }
        else{
            isPlaying = true;
            musicPlayListener.onPlay();
            changeSeekBar(sTime,eTime);
            playButton.setImageResource(R.drawable.ic_pause_black_large);
            pausePlayButtom.setImageResource(R.drawable.ic_pause_black_large);
        }


    }
    public void updateUI(BaiHat baiHat){
        timeBatdau.setText(String.format("%d : %d", 0, 0));
        sTime = 0;
        baiHatActive = baiHat;
        MyNotification.createNotification(getContext(),baiHatActive,isPlaying);
        musicPlayListener.onGetTime(baiHat);
        smallImageMusic.setImageBitmap(Library.StringToBitMap(baiHat.getAnh()));
        nameMusic.setText(baiHat.getmTenbaihat());
        artist.setText(baiHat.getmTencasi());
        bigImageMusic.setImageBitmap(Library.StringToBitMap(baiHat.getAnh()));
    }
    // laay bai hat da luu vao yêu thích và không yêu thích từ bộ nhớ
    private void loadFromCache(){
        ArrayList<BaiHat> bF = BaiHat.loadFromShared(getContext(), Constant.FAVORITE_MUSIC);
        ArrayList<BaiHat> bD = BaiHat.loadFromShared(getContext(), Constant.DIS_LIKE);
        if (bF != null){
            listFavorite = BaiHat.loadFromShared(getContext(), Constant.FAVORITE_MUSIC);
        }
        if (bD != null){
            listDisLike = BaiHat.loadFromShared(getContext(), Constant.DIS_LIKE);
        }
    }


    private ArrayList<BaiHat> getFavoriteMusic(){
        ArrayList<BaiHat> result;
        result = BaiHat.loadFromShared(getContext(), Constant.FAVORITE_MUSIC);
        return result;
    }

    private ArrayList<BaiHat> getDislikeMusic(){
        ArrayList<BaiHat> result;
        result = BaiHat.loadFromShared(getContext(), Constant.DIS_LIKE);
        return result;
    }
}
