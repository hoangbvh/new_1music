package huyencool.com.music2;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;

import huyencool.com.music2.controller.LayoutController;
import huyencool.com.music2.model.BaiHat;
import huyencool.com.music2.view.new_interface.CallBackHelper;

public class
MyNotification extends Notification  implements Parcelable {
    private static NotificationManager nManager;
    private static RemoteViews remoteView;
    public static ArrayList<BaiHat> listBaiHat;
    private static int posActive = -1;
    @SuppressLint("NewApi")
    private static final String  CHANNEL_ID = "SYMPER_1";
    public static void createNotification(Context context,BaiHat baiHat,Boolean isPlaying){
        Intent activityIntent = new Intent(context, LayoutController.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,0 , activityIntent,0);

        remoteView = new RemoteViews(context.getPackageName(), R.layout.notifications);
        remoteView.setTextViewText(R.id.baihat,baiHat.getmTenbaihat());
        remoteView.setTextViewText(R.id.casi, baiHat.getmTencasi());
        remoteView.setImageViewBitmap(R.id.ImageMusic,Library.StringToBitMap(baiHat.getAnh()));
        remoteView.setImageViewResource(R.id.nextButtom, R.drawable.ten2);
        remoteView.setImageViewResource(R.id.prevButtom, R.drawable.ic_rew_dark);
        if (isPlaying){
            remoteView.setImageViewResource(R.id.pauseButtom, R.drawable.ic_media_pause_light);
        }
        else{
            remoteView.setImageViewResource(R.id.pauseButtom, R.drawable.ic_media_play_light);
        }
        posActive = listBaiHat.indexOf(baiHat);


        NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(context,CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_pause_black_large)
                .setCustomContentView(remoteView)
                .setContentIntent(contentIntent);

        setListeners(remoteView,context,isPlaying);
        nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(1, nBuilder.build());
    }
    private static void setListeners(RemoteViews view,Context context, Boolean isPlaying){
        HelperActivity helperActivity = new HelperActivity();
        HelperActivity.listBaiHat = listBaiHat;
        Intent prev = new Intent(context, helperActivity.getClass());
        prev.setAction("prev");
        int posPrev = (posActive == 0) ? listBaiHat.size() - 1 : posActive - 1;
        prev.putExtra("pos",posPrev);
        PendingIntent prevButton = PendingIntent.getBroadcast(context,1,prev,PendingIntent.FLAG_CANCEL_CURRENT );
        view.setOnClickPendingIntent(R.id.prevButtom, prevButton);
        //listener 2
        Intent stop = new Intent(context, helperActivity.getClass());
        String action = (isPlaying) ? "pause" : "play";
        stop.setAction(action);
        stop.putExtra("pos",posActive);
        PendingIntent btn2 = PendingIntent.getBroadcast(context, 1, stop, PendingIntent.FLAG_CANCEL_CURRENT);
        view.setOnClickPendingIntent(R.id.pauseButtom, btn2);
        //listener 2
        Intent next = new Intent(context,  helperActivity.getClass());
        next.setAction("next");
        int posNext = (posActive == listBaiHat.size() - 1) ? 0 : posActive + 1;
        next.putExtra("pos",posNext);
        PendingIntent nextButton = PendingIntent.getBroadcast(context, 1, next, PendingIntent.FLAG_CANCEL_CURRENT);
        view.setOnClickPendingIntent(R.id.nextButtom, nextButton);
    }

}






























